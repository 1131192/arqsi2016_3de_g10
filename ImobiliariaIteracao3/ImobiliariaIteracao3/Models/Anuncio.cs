﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImobiliariaIteracao3.Models
{
    public enum TipoAnuncio
    {
        compra, venda, aluguer, permuta
    }

    public class Anuncio
    {
        public int AnuncioID { get; set; }

        public string UserID { get; set; }

        public string MediadorID { get; set; }

        [Display(Name = "Imóvel")]
        public int ImovelID { get; set; }

        [Display(Name = "Preço")]
        public float Preco { get; set; }

        public bool Estado { get; set; }

        [Display(Name = "Anúncio Correspondente")]
        public int? AnuncioCorrID { get; set; }

        public virtual Anuncio AnuncioCorr { get; set; }

        [Display(Name = "Tipo de Anúncio")]
        public TipoAnuncio TipoAnuncio { get; set; }

        public virtual Imovel Imovel { get; set; }
    }
}