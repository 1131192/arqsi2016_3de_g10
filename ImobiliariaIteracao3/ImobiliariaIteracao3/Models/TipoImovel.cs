﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImobiliariaIteracao3.Models
{
    public class TipoImovel
    {
        public int TipoImovelID { get; set; }

        [Display(Name = "Tipo Imóvel")]
        public string Nome { get; set; }

        public int? SubTipoID { get; set; }

        public virtual TipoImovel SubTipo { get; set; }
    }
}