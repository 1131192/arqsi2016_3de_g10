﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ImobiliariaIteracao3.Models
{
    public class Foto
    {
        [Key]
        public int ID { get; set; }
        public string url { get; set; }

        [Display(Name = "Imóvel")]
        [ForeignKey("Imovel")]
        public int ImovelId { get; set; }

        public virtual Imovel Imovel { get; set; }
    }
}