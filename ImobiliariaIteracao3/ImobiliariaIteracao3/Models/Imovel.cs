﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ImobiliariaIteracao3.Models
{
    public class Imovel
    {
        public int ImovelID { get; set; }

        public string UserID { get; set; }

        [Display(Name = "Tipo Imóvel")]
        public int TipoImovelID { get; set; }
        public int GPSID { get; set; }

        [Display(Name = "Área")]
        public float Area { get; set; }

        [Display(Name = "Localização")]
        [DisplayFormat(NullDisplayText = "Localização desconhecida")]
        public string Localizacao { get; set; }

        // Estado do imovel. 
        // True se estiver em algum anuncio
        // False se não estive
        public bool emAnuncio { get; set; }

        [DisplayFormat(NullDisplayText = "Sem coordenadas")]
        public virtual GPS GPS { get; set; }
        public virtual TipoImovel TipoImovel { get; set; }

        public virtual ICollection<Foto> Fotos { get; set; }
    }
}