﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImobiliariaIteracao3.Startup))]
namespace ImobiliariaIteracao3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
