﻿using ImobiliariaIteracao3.Helpers;
using ImobiliariaIteracao3.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ImobiliariaIteracao3.Controllers
{
    public class TipoImovelController : Controller
    {
        // GET: TipoImovel
        //[Authorize]
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                return View(tiposImovel);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: TipoImovel/Details/5
        //[Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null) return HttpNotFound();
                return View(tipoImovel);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        // GET: TipoImovel/Create
        //[Authorize(Roles = "Admin")]
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                ViewBag.TipoImovelID = new SelectList(tiposImovel, "TipoImovelID", "Nome");
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // POST: TipoImovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TipoImovelID,Nome,SubTipoID")] TipoImovel tipoImovel)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string tipoImovelJSON = JsonConvert.SerializeObject(tipoImovel);
                HttpContent content = new StringContent(tipoImovelJSON,
                            System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/TipoImovel", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: TipoImovel/Edit/5
        //[Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();
            
            // Vai buscar todos os tipos de imóvel para, posteriormente, ser feita a filtragem
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                
                // Filtra os tipos de imóvel disponiveis para a escolha do utilizador
                List<TipoImovel> lista = getListaTipos(id.Value, tiposImovel.ToList());
                ViewBag.TipoImovelID = new SelectList(lista, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            HttpResponseMessage response2 = await client.GetAsync("api/TipoImovel/" + id);
            if (response2.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null) return HttpNotFound();
                return View(tipoImovel);
            }
            return Content("Ocorreu um erro: " + response2.StatusCode);

        }

        // POST: TipoImovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TipoImovelID,Nome,SubTipoID")] TipoImovel tipoImovel)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string tipoImovelJSON = JsonConvert.SerializeObject(tipoImovel);
                HttpContent content = new StringContent(tipoImovelJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response =
                        await client.PutAsync("api/TipoImovel/" + tipoImovel.TipoImovelID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: TipoImovel/Delete/5
        //[Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tipoImovel = JsonConvert.DeserializeObject<TipoImovel>(content);
                if (tipoImovel == null) return HttpNotFound();
                return View(tipoImovel);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: TipoImovel/Delete/5
        //[Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/TipoImovel/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("../TipoImovel/DeleteErrorView");
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        // Filtra os tipos de imóvel a apresentar
        public List<TipoImovel> getListaTipos(int id, List<TipoImovel> lista)
        {
            List<TipoImovel> listaFinal = new List<TipoImovel>();

            foreach (var ti in lista)
            {
                if (ti.SubTipoID != id && ti.TipoImovelID != id)
                {
                    listaFinal.Add(ti);
                }
            }

            return listaFinal;
        }
    }
}