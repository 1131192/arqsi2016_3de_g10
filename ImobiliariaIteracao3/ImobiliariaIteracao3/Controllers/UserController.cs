﻿using ImobiliariaIteracao3.Helpers;
using ImobiliariaIteracao3.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ImobiliariaIteracao3.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();

            HttpResponseMessage response = await client.GetAsync("api/Role");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var roles =
                    JsonConvert.DeserializeObject<IEnumerable<IdentityRole>>(content);
                ViewBag.Roles = roles.ToList();
            }

            HttpResponseMessage response2 = await client.GetAsync("api/User");
            if (response2.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var users =
                    JsonConvert.DeserializeObject<IEnumerable<ApplicationUser>>(content);

                return View(users);
            }
            else
            {
                return Content("Ocorreu um erro: " + response2.StatusCode);
            }
        }

        // GET: User/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();

            // Vai buscar todos os roles
            HttpResponseMessage response = await client.GetAsync("api/Role");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var roles =
                    JsonConvert.DeserializeObject<IEnumerable<IdentityRole>>(content);

                ViewBag.Roles = roles.ToList();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            // Vai buscar o User
            HttpResponseMessage response2 = await client.GetAsync("api/User/" + id);
            if (response2.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var user = JsonConvert.DeserializeObject<ApplicationUser>(content);
                if (user == null) return HttpNotFound();
                return View(user);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }


        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,AccessFailedCount,Claims,Email,EmailConfirmed,LockoutEnabled,LockoutEndDateUtc,Logins,PasswordHash,PhoneNumber,PhoneNumberConfirmed,Roles,SecurityStamp,TwoFactorEnabled,UserName")] ApplicationUser user)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string userJSON = JsonConvert.SerializeObject(user);
                HttpContent content = new StringContent(userJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response =
                        await client.PutAsync("api/User/" + user.Id, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
    }
}