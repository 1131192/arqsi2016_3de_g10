﻿using ImobiliariaIteracao3.Helpers;
using ImobiliariaIteracao3.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ImobiliariaIteracao3.Controllers
{
    public class AlertaController : Controller
    {
        // GET: Alerta
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alerta/");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alertas =
                    JsonConvert.DeserializeObject<IEnumerable<Alerta>>(content);
                return View(alertas);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Alerta/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alerta/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                if (alerta == null) return HttpNotFound();
                return View(alerta);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Alerta/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);
                ViewBag.TipoImovelID = getTiposImovel(tiposImovel.ToList());
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // POST: Alerta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AlertaID,TipoImovelID,TipoAnuncio,Area,Preco,DataCriacao")] Alerta alerta)
        {
            try
            {
                alerta.DataCriacao = DateTime.Now;
                //alerta.UserID = System.Web.HttpContext.Current.User.Identity.GetUserId();

                var client = WebApiHttpClient.GetClient();
                string alertaJSON = JsonConvert.SerializeObject(alerta);
                HttpContent content = new StringContent(alertaJSON,
                            System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Alerta", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Alerta/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();
            
            // Vai buscar todos os imóveis
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);

                ViewBag.TipoImovelID = getTiposImovel(tiposImovel.ToList());
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            HttpResponseMessage response2 = await client.GetAsync("api/Alerta/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                if (alerta == null) return HttpNotFound();
                return View(alerta);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Alerta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AlertaID,UserID,TipoImovelID,TipoAnuncio,Area,Preco,DataCriacao")] Alerta alerta)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string alertaJSON = JsonConvert.SerializeObject(alerta);
                HttpContent content = new StringContent(alertaJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response =
                        await client.PutAsync("api/Alerta/" + alerta.AlertaID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Alerta/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alerta/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerta = JsonConvert.DeserializeObject<Alerta>(content);
                if (alerta == null) return HttpNotFound();
                return View(alerta);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Alerta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Alerta/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private List<SelectListItem> getTiposImovel(List<TipoImovel> tiposImovel)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var tipoImovel in tiposImovel)
            {
                string str;

                if (tipoImovel.SubTipo != null)
                {
                    str = tipoImovel.SubTipo.Nome + ", " +
                                tipoImovel.Nome;
                }
                else
                {
                    str = tipoImovel.Nome;
                }

                items.Add(new SelectListItem { Text = str, Value = tipoImovel.TipoImovelID.ToString() });
            }
            return items;
        }
    }
}