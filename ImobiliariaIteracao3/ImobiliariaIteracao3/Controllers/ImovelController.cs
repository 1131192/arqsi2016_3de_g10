﻿using ImobiliariaIteracao3.Helpers;
using ImobiliariaIteracao3.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ImobiliariaIteracao3.Controllers
{
    public class ImovelController : Controller
    {
        // GET: Imovel
        public async Task<ActionResult> Index()
        {
            Session["CurrentUrl"] = Request.Url.ToString();
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imoveis =
                    JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);
                return View(imoveis);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Imovel/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovel = JsonConvert.DeserializeObject<Imovel>(content);
                if (imovel == null) return HttpNotFound();
                return View(imovel);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Imovel/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);

                ViewBag.TipoImovelID = getTiposImovel(tiposImovel.ToList());
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // POST: Imovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ImovelID,TipoImovelID,GPSID,Area,Localizacao")] Imovel imovel,
            [Bind(Include = "GPSID,Latitude,Longitude,Altitude")] GPS gps)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                
                // Guardar objeto GPS
                string gpsJSON = JsonConvert.SerializeObject(gps);
                HttpContent content = new StringContent(gpsJSON,
                            System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/GPS", content);
               
                if (response.IsSuccessStatusCode)
                {
                    string contentTemp = await response.Content.ReadAsStringAsync();
                    var gps2 = JsonConvert.DeserializeObject<GPS>(contentTemp);

                    imovel.GPSID = gps2.GPSID;
                    //imovel.UserID = System.Web.HttpContext.Current.User.Identity.GetUserId();

                    // Guardar objeto Imóvel
                    string imovelJSON = JsonConvert.SerializeObject(imovel);
                    HttpContent content2 = new StringContent(imovelJSON,
                                System.Text.Encoding.Unicode, "application/json");
                    var response2 = await client.PostAsync("api/Imovel", content2);

                    if (response2.IsSuccessStatusCode)
                    {
                        return Session["CurrentUrl"] == null ?
                                Redirect("Index") :
                                Redirect(Session["CurrentUrl"].ToString());
                    }
                    else
                    {
                        return Content("Ocorreu um erro: " + response2.StatusCode);
                    }
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Imovel/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();

            // Vai buscar todos os tipos de imóvel
            HttpResponseMessage response = await client.GetAsync("api/TipoImovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var tiposImovel =
                    JsonConvert.DeserializeObject<IEnumerable<TipoImovel>>(content);

                ViewBag.TipoImovelID = new SelectList(tiposImovel, "TipoImovelID", "Nome");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            HttpResponseMessage response2 = await client.GetAsync("api/Imovel/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var imovel = JsonConvert.DeserializeObject<Imovel>(content);
                if (imovel == null) return HttpNotFound();
                return View(imovel);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Imovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ImovelID,UserID,TipoImovelID,GPSID,Area,Localizacao,emAnuncio")] Imovel imovel,
            [Bind(Include = "GPSID,Latitude,Longitude,Altitude")] GPS gps)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                
                // Guarda alterações ao objeto GPS
                string gpsJSON = JsonConvert.SerializeObject(gps);
                HttpContent content = new StringContent(gpsJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response =
                        await client.PutAsync("api/GPS/" + gps.GPSID, content);

                // Guarda alterações ao objeto Imovel
                string imovelJSON = JsonConvert.SerializeObject(imovel);
                HttpContent content2 = new StringContent(imovelJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response2 =
                        await client.PutAsync("api/Imovel/" + imovel.ImovelID, content2);

                if (response.IsSuccessStatusCode && response2.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Imovel/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imovel = JsonConvert.DeserializeObject<Imovel>(content);
                if (imovel == null) return HttpNotFound();
                return View(imovel);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Imovel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Imovel/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private List<SelectListItem> getTiposImovel(List<TipoImovel> tiposImovel)
        {
            //var tiposImovel = db.TiposImovel.ToList();
            //var tiposImovel = repoTipoImovel.GetData();

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var tipoImovel in tiposImovel)
            {
                string str;

                if (tipoImovel.SubTipo != null)
                {
                    str = tipoImovel.SubTipo.Nome + ", " +
                                tipoImovel.Nome;
                }
                else
                {
                    str = tipoImovel.Nome;
                }

                items.Add(new SelectListItem { Text = str, Value = tipoImovel.TipoImovelID.ToString() });
            }
            return items;
        }
    }
}