﻿using ImobiliariaIteracao3.Helpers;
using ImobiliariaIteracao3.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ImobiliariaIteracao3.Controllers
{
    public class FotoController : Controller
    {
        // GET: Foto
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Foto");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var fotos =
                    JsonConvert.DeserializeObject<IEnumerable<Foto>>(content);
                return View(fotos);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Foto/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Foto/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var foto = JsonConvert.DeserializeObject<Foto>(content);
                if (foto == null) return HttpNotFound();
                return View(foto);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Foto/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imoveis =
                    JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);
                ViewBag.ImovelID = getListaImoveis(imoveis.ToList());
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // POST: Foto/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,url,ImovelId")] Foto foto)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string fotoJSON = JsonConvert.SerializeObject(foto);
                HttpContent content = new StringContent(fotoJSON,
                            System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Foto", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Foto/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = WebApiHttpClient.GetClient();

            // Vai buscar todos imóveis
            HttpResponseMessage response = await client.GetAsync("api/Imovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imoveis =
                    JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);

                ViewBag.ImovelID = getListaImoveis(imoveis.ToList());
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            HttpResponseMessage response2 = await client.GetAsync("api/Foto/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var foto = JsonConvert.DeserializeObject<Foto>(content);
                if (foto == null) return HttpNotFound();
                return View(foto);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Foto/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,url,ImovelId")] Foto foto)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string fotoJSON = JsonConvert.SerializeObject(foto);
                HttpContent content = new StringContent(fotoJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response =
                        await client.PutAsync("api/Foto/" + foto.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Foto/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Foto/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var foto = JsonConvert.DeserializeObject<Foto>(content);
                if (foto == null) return HttpNotFound();
                return View(foto);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Foto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Foto/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private List<SelectListItem> getListaImoveis(List<Imovel> temp)
        {
            // Mostrar apenas os imoveis que o cliente criou
            //var temp = db.Imoveis.Include(i => i.GPS).Include(i => i.TipoImovel);
            /*List<Imovel> imoveis = new List<Imovel>();

            foreach (var imovel in temp)
            {
                if (imovel.UserID == User.Identity.GetUserId())
                {
                    imoveis.Add(imovel);
                }
            }*/

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var imovel in temp)
            {
                string str = imovel.TipoImovel.Nome + ", " +
                                imovel.Area + "m2, " +
                                imovel.Localizacao;

                items.Add(new SelectListItem { Text = str, Value = imovel.ImovelID.ToString() });
            }
            return items;
        }
        /*
        private List<Foto> getListaFotos()
        {
            var temp = db.Fotos.Include(f => f.Imovel);
            List<Foto> fotos = new List<Foto>();

            foreach (var item in temp)
            {
                if (item.Imovel.UserID == User.Identity.GetUserId())
                {
                    fotos.Add(item);
                }
            }

            return fotos;
        }*/
    }
}