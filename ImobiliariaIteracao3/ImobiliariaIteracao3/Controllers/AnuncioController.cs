﻿using ImobiliariaIteracao3.Helpers;
using ImobiliariaIteracao3.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ImobiliariaIteracao3.Controllers
{
    public class AnuncioController : Controller
    {
        // GET: Anuncio
        //[Authorize]
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncios =
                    JsonConvert.DeserializeObject<IEnumerable<Anuncio>>(content);
                return View(anuncios);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Anuncio/Details/5
        //[Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<Anuncio>(content);
                if (anuncio == null) return HttpNotFound();
                return View(anuncio);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Anuncio/Create
        //[Authorize]
        public async Task<ActionResult> Create()
        {
            Session["CurrentUrl"] = Request.Url.ToString();

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Imovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imoveis =
                    JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);

                ViewBag.ImovelID = getListaImoveis(imoveis.ToList());
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // POST: Anuncio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AnuncioID,ImovelID,Preco,TipoAnuncio")] Anuncio anuncio)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string anuncioJSON = JsonConvert.SerializeObject(anuncio);
                HttpContent content = new StringContent(anuncioJSON,
                            System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncio", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Anuncio/Edit/5
        //[Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Session["CurrentUrl"] = Request.Url.ToString();

            var client = WebApiHttpClient.GetClient();

            // Vai buscar todos os imóveis
            HttpResponseMessage response = await client.GetAsync("api/Imovel");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var imoveis =
                    JsonConvert.DeserializeObject<IEnumerable<Imovel>>(content);

                ViewBag.ImovelID = getListaImoveis(imoveis.ToList());
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            HttpResponseMessage response2 = await client.GetAsync("api/Anuncio/" + id);

            if (response2.IsSuccessStatusCode)
            {
                string content = await response2.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<Anuncio>(content);
                if (anuncio == null) return HttpNotFound();
                return View(anuncio);
            }

            return Content("Ocorreu um erro: " + response2.StatusCode);
        }

        // POST: Anuncio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AnuncioID,UserID,ImovelID,Preco,TipoAnuncio")] Anuncio anuncio)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string anuncioJSON = JsonConvert.SerializeObject(anuncio);
                HttpContent content = new StringContent(anuncioJSON,
                                System.Text.Encoding.Unicode, "application/json");
                var response =
                        await client.PutAsync("api/Anuncio/" + anuncio.AnuncioID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Anuncio/Delete/5
        //[Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<Anuncio>(content);
                if (anuncio == null) return HttpNotFound();
                return View(anuncio);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Anuncio/Delete/5
        //[Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Anuncio/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private List<SelectListItem> getListaImoveis(List<Imovel> temp)
        {
            //List<Imovel> imoveis = new List<Imovel>();
            /*
            foreach (var imovel in temp)
            {
                if (imovel.UserID == User.Identity.GetUserId())
                {
                    imoveis.Add(imovel);
                }
            }*/

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var imovel in temp)
            {
                string str = imovel.TipoImovel.Nome + ", " +
                                imovel.Area + "m2, " +
                                imovel.Localizacao;

                items.Add(new SelectListItem { Text = str, Value = imovel.ImovelID.ToString() });
            }
            return items;
        }
    }
}