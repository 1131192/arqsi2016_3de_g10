﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImobiliariaIteracao2.Startup))]
namespace ImobiliariaIteracao2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
