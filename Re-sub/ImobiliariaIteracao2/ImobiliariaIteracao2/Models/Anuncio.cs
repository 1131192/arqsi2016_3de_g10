﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ImobiliariaIteracao2.Models
{
    public enum TipoAnuncio
    {
        compra, venda, aluguer, permuta
    }

    public class Anuncio
    {
        public int AnuncioID { get; set; }

        public string UserID { get; set; }

        [Display(Name = "Imóvel")]
        public int ImovelID { get; set; }

        [Display(Name = "Preço")]
        public float Preco { get; set; }

        [Display(Name = "Tipo de Anúncio")]
        public TipoAnuncio TipoAnuncio { get; set; }

        public virtual Imovel Imovel { get; set; }
    }
}