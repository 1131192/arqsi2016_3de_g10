﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ImobiliariaIteracao2.Models
{
    public class Foto
    {
        [Key]
        public int ID { get; set; }
        public string url { get; set; }

        [Display(Name = "Imóvel")]
        [ForeignKey("Imovel")]
        public int ImovelId { get; set; }

        public virtual Imovel Imovel { get; set; }

    }
}