﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImobiliariaIteracao2.DAL;
using ImobiliariaIteracao2.Models;
using Microsoft.AspNet.Identity;

namespace ImobiliariaIteracao2.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class AnuncioController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        // GET: Anuncio
        public ActionResult Index()
        {
            var anuncios = db.Anuncios.Include(a => a.Imovel);

            return View(anuncios.ToList());
        }

        // GET: Anuncio/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            return View(anuncio);
        }

        // GET: Anuncio/Create
        public ActionResult Create()
        {
            //ViewBag.ImovelID = new SelectList(db.Imoveis, "ImovelID", "ImovelID");
            //ViewBag.ImovelID = new SelectList(imoveis, "ImovelID", "ImovelID");
            ViewBag.ImovelID = getListaImoveis();

            Session["CurrentUrl"] = Request.Url.ToString();
            return View();
        }

        // POST: Anuncio/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnuncioID,ImovelID,Preco,TipoAnuncio")] Anuncio anuncio)
        {
            if (ModelState.IsValid)
            {
                // O Imovel passa a estar presente num anuncio
                //anuncio.Imovel.emAnuncio = true;

                anuncio.UserID = User.Identity.GetUserId();
                db.Anuncios.Add(anuncio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ImovelID = getListaImoveis();
            return View(anuncio);
        }

        // GET: Anuncio/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            ViewBag.ImovelID = getListaImoveis();

            // Guardar o url da página atual
            Session["CurrentUrl"] = Request.Url.ToString();

            // Verifica se o utilizador que tentar editar é o mesmo que criou o anúncio
            // Caso seja, permite a edição.
            // Caso não seja, bloqueia.
            if (anuncio.UserID == User.Identity.GetUserId())
            {
                return View(anuncio);
            }
            return View("../Shared/PermissionErrorView");
        }

        // POST: Anuncio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnuncioID,UserID,ImovelID,Preco,TipoAnuncio")] Anuncio anuncio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(anuncio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ImovelID = getListaImoveis();
            return View(anuncio);
        }

        // GET: Anuncio/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            // Verifica se o utilizador que tentar apagar é o mesmo que criou o anúncio
            // Caso seja, permite a edição.
            // Caso não seja, bloqueia.
            if (anuncio.UserID == User.Identity.GetUserId())
            {
                return View(anuncio);
            }
            return View("../Shared/PermissionErrorView");
        }

        // POST: Anuncio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Anuncio anuncio = db.Anuncios.Find(id);
            db.Anuncios.Remove(anuncio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private List<SelectListItem> getListaImoveis()
        {
            // Mostrar apenas os imoveis que o cliente criou
            var temp = db.Imoveis.Include(i => i.GPS).Include(i => i.TipoImovel);
            List<Imovel> imoveis = new List<Imovel>();

            foreach (var imovel in temp)
            {
                if (imovel.UserID == User.Identity.GetUserId())
                {
                    imoveis.Add(imovel);
                }
            }

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var imovel in imoveis)
            {
                string str = imovel.TipoImovel.Nome + ", " +
                                imovel.Area + "m2, " +
                                imovel.Localizacao;

                items.Add(new SelectListItem { Text = str, Value = imovel.ImovelID.ToString() });
            }
            return items;
        }
    }
}
