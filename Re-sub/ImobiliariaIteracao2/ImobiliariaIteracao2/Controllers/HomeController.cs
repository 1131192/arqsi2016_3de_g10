﻿using ImobiliariaIteracao2.DAL;
using ImobiliariaIteracao2.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ImobiliariaIteracao2.Controllers
{
    public class HomeController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }

            return View("../Shared/WidgetHomePageView");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}