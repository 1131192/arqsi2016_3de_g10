﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImobiliariaIteracao2.DAL;
using ImobiliariaIteracao2.Models;
using Microsoft.AspNet.Identity;

namespace ImobiliariaIteracao2.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class AlertaController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        // GET: Alerta
        public ActionResult Index()
        {
            var alertas = getAlertas();
            return View(alertas);
        }

        // GET: Alerta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alerta/Create
        public ActionResult Create()
        {
            ViewBag.TipoImovelID = getTiposImovel();
            return View();
        }

        // POST: Alerta/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlertaID,UserID,TipoImovelID,TipoAnuncio,Area,Preco,DataCriacao")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                alerta.DataCriacao = DateTime.Now;
                alerta.UserID = User.Identity.GetUserId();

                db.Alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoImovelID = getTiposImovel();
            return View(alerta);
        }

        // GET: Alerta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            //ViewBag.AnuncioID = new SelectList(db.Anuncios, "AnuncioID", "UserID", alerta.AnuncioID);
            ViewBag.TipoImovelID = getTiposImovel();
            return View(alerta);
        }

        // POST: Alerta/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlertaID,UserID,TipoImovelID,TipoAnuncio,Area,Preco,DataCriacao")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoImovelID = getTiposImovel();
            return View(alerta);
        }

        // GET: Alerta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alerta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.Alertas.Find(id);
            db.Alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // Retorna os alertas de um utilizador
        private List<Alerta> getAlertas()
        {
            var temp = db.Alertas.Include(a => a.TipoImovel).ToList();
            List<Alerta> alertas = new List<Alerta>();

            foreach (var alerta in temp)
            {
                if (alerta.UserID == User.Identity.GetUserId())
                {
                    alertas.Add(alerta);
                }
            }
            return alertas;
        }

        private List<SelectListItem> getTiposImovel()
        {
            var tiposImovel = db.TiposImovel.ToList();
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var tipoImovel in tiposImovel)
            {
                string str;

                if (tipoImovel.SubTipo != null)
                {
                    str = tipoImovel.SubTipo.Nome + ", " +
                                tipoImovel.Nome;
                }
                else
                {
                    str = tipoImovel.Nome;
                }

                items.Add(new SelectListItem { Text = str, Value = tipoImovel.TipoImovelID.ToString() });
            }
            return items;
        }
    }
}
