﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImobiliariaIteracao2.DAL;
using ImobiliariaIteracao2.Models;

namespace ImobiliariaIteracao2.Controllers
{
    public class TipoImovelController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        // GET: TipoImovel
        [Authorize]
        public ActionResult Index()
        {
            return View(db.TiposImovel.ToList());
        }

        // GET: TipoImovel/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoImovel tipoImovel = db.TiposImovel.Find(id);
            if (tipoImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoImovel);
        }

        // GET: TipoImovel/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.TipoImovelID = new SelectList(db.TiposImovel, "TipoImovelID", "Nome");
            return View();
        }

        // POST: TipoImovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoImovelID,Nome,SubTipoID")] TipoImovel tipoImovel)
        {
            if (ModelState.IsValid)
            {
                db.TiposImovel.Add(tipoImovel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoImovel);
        }

        // GET: TipoImovel/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoImovel tipoImovel = db.TiposImovel.Find(id);
            ViewBag.TipoImovelID = new SelectList(getListaTipos(tipoImovel.TipoImovelID), "TipoImovelID", "Nome");
            if (tipoImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoImovel);
        }

        // POST: TipoImovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoImovelID,Nome,SubTipoID")] TipoImovel tipoImovel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoImovel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoImovel);
        }

        // GET: TipoImovel/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoImovel tipoImovel = db.TiposImovel.Find(id);
            if (tipoImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoImovel);
        }

        // POST: TipoImovel/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoImovel ti = db.TiposImovel.Find(id);
            List<TipoImovel> lista = db.TiposImovel.ToList();

            foreach (var tipoImovel in lista)
            {
                if (tipoImovel.SubTipoID == ti.TipoImovelID)
                {
                    return View("../TipoImovel/DeleteErrorView");
                }
            }

            db.TiposImovel.Remove(ti);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<TipoImovel> getListaTipos(int id)
        {
            List<TipoImovel> lista = db.TiposImovel.ToList();
            List<TipoImovel> listaFinal = new List<TipoImovel>();

            foreach (var ti in lista)
            {
                if (ti.SubTipoID != id && ti.TipoImovelID != id)
                {
                    listaFinal.Add(ti);
                }
            }

            return listaFinal;
        }
    }
}
