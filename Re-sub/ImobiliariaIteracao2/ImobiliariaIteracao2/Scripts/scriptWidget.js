function iniciar(nome) {

    var facetas = [];
    var valoresFaceta = [];
    var tiposFaceta = [];
    var resultados = [];
    var checkedElements = [];
    var nomeElemento = nome;
    var facetaTemp = null;
    var xmlHttpObj = null;
    var valorMaximo = null;
    var valorMinimo = null;
    var stringPesquisa = "";


    getFacetasAJAX();
    construirFacetas();
    pesquisa();

    function getFacetasAJAX() {
        xmlHttpObj = new XMLHttpRequest();
        if (xmlHttpObj) {
            xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php", false);
            xmlHttpObj.onreadystatechange = getFacetasXML;
            xmlHttpObj.send(null);
        }
    }

    function getFacetasXML() {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            var xml = xmlHttpObj.responseXML;
            var facetasXML = xml.getElementsByTagName("faceta");

            for (var i = 0; i < facetasXML.length; i++) {
                var textNode = facetasXML[i].childNodes[0];
                facetas[i] = textNode.nodeValue;
            }
        }
    }

    function construirFacetas() {

        construirBase();

        var tabelaPesquisa = document.createElement("table");
        tabelaPesquisa.id = nomeElemento + "_tabelaPesquisa";
        tabelaPesquisa.className = "tabelaPesquisa";

        for (var i = 0; i < facetas.length; i++) {
            // Buscar os valores da faceta e guardar num array
            getValoresFacetaAJAX(facetas[i]);

            // Buscar o tipo da faceta e guardar numa variavel global
            getTipoFacetaAJAX(facetas[i]);

            //verifica o tipo da faceta
            if (tiposFaceta[i].semântica != "figura") {
                //var colunaPesquisa = document.getElementById("colSearch");

                var tr = document.createElement("tr");

                // Criar nome Faceta
                var tdNome = document.createElement("td");
                tdNome.id = nomeElemento + "_" + facetas[i];
                tdNome.appendChild(document.createTextNode(facetas[i]));

                tr.appendChild(tdNome);

                if (tiposFaceta[i].discreto == "discreto") {
                    construirFacetaDiscreta(facetas[i], tr);
                } else {
                    construirFacetaContinua(facetas[i], tr);
                }

                tabelaPesquisa.appendChild(tr);
            }
        }

        var divPesquisa = document.getElementById(nomeElemento + "_colSearch");
        divPesquisa.appendChild(tabelaPesquisa);
    }

    function construirBase() {
        var body = document.getElementById(nomeElemento);

        var divContainer = document.createElement("div");
        var divSearch = document.createElement("div");
        var divResults = document.createElement("div");

        divContainer.id = nomeElemento + "_divContainer";
        divSearch.id = nomeElemento + "_colSearch";
        divResults.id = nomeElemento + "_colResults";

        divContainer.className = "divContainer";
        divSearch.className = "divPesquisa";
        divResults.className = "divResultados";

        divContainer.appendChild(divSearch);
        divContainer.appendChild(divResults);

        body.appendChild(divContainer);
    }

    function construirFacetaDiscreta(faceta, tr) {

        var tdCheckbox = document.createElement("td");

        for (var i = 0; i < valoresFaceta[faceta].length; i++) {

            var noBr1 = document.createElement("br");

            var checkbox = document.createElement("input");
            checkbox.id = nomeElemento + "_" + valoresFaceta[faceta][i];
            checkbox.value = valoresFaceta[faceta][i];
            checkbox.type = "checkbox";
            checkbox.addEventListener("click", pesquisa);

            var label = document.createElement("label");
            label.for = nomeElemento + "_" + valoresFaceta[faceta][i];
            label.appendChild(document.createTextNode(valoresFaceta[faceta][i]));

            tdCheckbox.appendChild(checkbox);
            tdCheckbox.appendChild(label);
            tdCheckbox.appendChild(noBr1);

        }
        var noBr2 = document.createElement("br");

        tdCheckbox.appendChild(noBr2);
        tr.appendChild(tdCheckbox);
    }

    function construirFacetaContinua(faceta, tr) {
        // Buscar o valor maximo e minimo desta faceta continua
        getValorMaximoAJAX(faceta);
        getValorMinimoAJAX(faceta);

        var elem = document.getElementById(nomeElemento + "_colSearch");

        var noBr = document.createElement("br");

        var tdInput = document.createElement("td");

        var inputElemNumber = document.createElement("input");
        inputElemNumber.type = "number";
        inputElemNumber.step = 20;
        inputElemNumber.max = valorMaximo;
        inputElemNumber.min = valorMinimo;
        inputElemNumber.addEventListener("change", pesquisa);

        tdInput.appendChild(inputElemNumber);
        tdInput.appendChild(noBr);
        tr.appendChild(tdInput);
    }

    function getTipoFacetaAJAX(faceta) {
        xmlHttpObj = new XMLHttpRequest();
        if (xmlHttpObj) {
            facetaTemp = faceta;
            xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=" + faceta, false);
            xmlHttpObj.onreadystatechange = getTipoFacetaJSON;
            xmlHttpObj.send(null);
        }
    }

    function getTipoFacetaJSON() {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            var jsonText = xmlHttpObj.responseText;
            var tipoFaceta = JSON.parse(jsonText);

            tiposFaceta.push(tipoFaceta);
        }
    }

    function getValoresFacetaAJAX(faceta) {
        xmlHttpObj = new XMLHttpRequest();
        if (xmlHttpObj) {
            facetaTemp = faceta;
            xmlHttpObj.onreadystatechange = getValoresFacetaJSON;
            xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=" + faceta, false);
            xmlHttpObj.send(null);
        }
    }

    function getValoresFacetaJSON() {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            var jsonText = xmlHttpObj.responseText;
            var valores = JSON.parse(jsonText);
            //alert(jsonText);
            valoresFaceta[facetaTemp] = valores;
        }
    }

    function getValorMaximoAJAX(faceta) {
        xmlHttpObj = new XMLHttpRequest();
        if (xmlHttpObj) {
            facetaTemp = faceta;
            xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/maxFaceta.php?facetaCont=" + faceta, false);
            xmlHttpObj.onreadystatechange = getValorMaximoJSON;
            xmlHttpObj.send(null);
        }
    }

    function getValorMaximoJSON() {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            var jsonText = xmlHttpObj.responseText;
            var valorMaxTemp = JSON.parse(jsonText);

            valorMaximo = parseInt(valorMaxTemp.max);
        }
    }

    function getValorMinimoAJAX(faceta) {
        xmlHttpObj = new XMLHttpRequest();
        if (xmlHttpObj) {
            facetaTemp = faceta;
            xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/minFaceta.php?facetaCont=" + faceta, false);
            xmlHttpObj.onreadystatechange = getValorMinimoJSON;
            xmlHttpObj.send(null);
        }
    }

    function getValorMinimoJSON() {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            var jsonText = xmlHttpObj.responseText;
            var valorMinTemp = JSON.parse(jsonText);

            valorMinimo = parseInt(valorMinTemp.min);
        }
    }

    function construirStringPesquisa(nomeFaceta) {
        stringPesquisa += nomeFaceta + "=";

        for (var i = 0; i < checkedElements.length; i++) {
            stringPesquisa += checkedElements[i];
            if (i < checkedElements.length - 1) {
                stringPesquisa += ",";
            }
        }

        stringPesquisa += "&";
    }

    function pesquisa() {
        clearResultados();
        //document.getElementById(nomeElemento + "_colResults").innerHTML = "";
        getCheckedElements();
        fazerPesquisaAJAX();
    }

    function clearResultados() {
        var col = document.getElementById(nomeElemento + "_colResults");
        while (col.hasChildNodes()) {
            col.removeChild(col.firstChild);
        }
    }

    function fazerPesquisaAJAX() {
        //if (stringPesquisa) {
        //alert(nomeElemento);
        xmlHttpObj = new XMLHttpRequest();
        if (xmlHttpObj) {
            xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?" + stringPesquisa, true);
            xmlHttpObj.onreadystatechange = fazerPesquisaJSON;
            xmlHttpObj.send(null);
        }
        //}
    }

    function fazerPesquisaJSON() {
        if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) {
            // Restaurar a string de pesquisa
            stringPesquisa = "";
            //alert(nomeElemento);
            var jsonText = xmlHttpObj.responseText;
            resultados = JSON.parse(jsonText);
            if (resultados.length === 0) {
                construirMensagemErro();
            } else {
                construirResultados();
            }
        }
    }

    function getAllCheckboxes() {
        var array = [];

        //retorna um array com todos os elementos tr da tabela
        var elems = document.getElementById(nomeElemento + "_tabelaPesquisa").childNodes;

        for (var i = 0; i < elems.length; i++) {
            var elemsTd = elems[i].lastChild.childNodes;
            for (var j = 0; j < elemsTd.length; j++) {
                if (elemsTd[j].type == "checkbox") {
                    array.push(elemsTd[j]);
                }
            }
        }

        return array;
    }

    function getCheckedElements() {
        var nomeFaceta = null;

        //retorna um array com todos os elementos tr da tabela
        var elems = document.getElementById(nomeElemento + "_tabelaPesquisa").childNodes;

        for (var i = 0; i < elems.length; i++) {
            var elemsTd = elems[i].lastChild.childNodes;
            nomeFaceta = elems[i].firstChild.innerText;

            for (var j = 0; j < elemsTd.length; j++) {
                if (elemsTd[j].type == "checkbox" &&
                    elemsTd[j].checked === true) {

                    checkedElements.push(elemsTd[j].value);
                }

                if (elemsTd[j].type == "number" &&
                    (elemsTd[j].value < valorMaximo &&
                        elemsTd[j].value > valorMinimo)) {
                    checkedElements.push(elemsTd[j].value);
                }
            }

            if (checkedElements.length > 0) {
                construirStringPesquisa(nomeFaceta);
            }
            checkedElements = [];
        }

    }

    function construirMensagemErro() {
        var colResultados = document.getElementById(nomeElemento + "_colResults");

        var img = document.createElement("img");
        img.src = "http://i.imgur.com/wYwcM9G.png";

        colResultados.appendChild(img);
    }

    function construirResultados() {
        var colResultados = document.getElementById(nomeElemento + "_colResults");
        for (var i = 0; i < resultados.length; i++) {

            var table = document.createElement("table");
            var tr = document.createElement("tr");
            var tdImagens = document.createElement("td");
            var tdValores = document.createElement("td");

            tdImagens.className = "tdresults";
            tdValores.className = "tdresults";

            for (var j = 0; j < facetas.length; j++) {

                if (tiposFaceta[j].semântica == "figura") {

                    if (resultados[i][facetas[j]].length > 0) {

                        for (var k = 0; k < resultados[i][facetas[j]].length; k++) {
                            var img = document.createElement("img");
                            img.src = resultados[i][facetas[j]][k];

                            tdImagens.appendChild(img);
                        }
                    } else {
                        var img = document.createElement("img");
                        img.src = "http://i.imgur.com/iTdAUSR.png";

                        tdImagens.appendChild(img);
                    }
                } else {
                    var ul = document.createElement("ul");

                    var li = document.createElement("li");
                    var label = document.createElement("label");

                    label.appendChild(document.createTextNode(facetas[j] + ": " + resultados[i][facetas[j]]));
                    li.appendChild(label);
                    ul.appendChild(li);
                }

                tdValores.appendChild(ul);
            }

            tr.appendChild(tdImagens);
            tr.appendChild(tdValores);
            table.appendChild(tr);
            colResultados.appendChild(table);
        }
        disableElements();
    }

    function disableElements() {
        var array = getAllCheckboxes();
        var arrTemp = [];
        for (var i = 0; i < array.length; i++) {
            for (var j = 0; j < facetas.length; j++) {
                for (var k = 0; k < resultados.length; k++) {
                    if (array[i].value == resultados[k][facetas[j]]) {
                        arrTemp.push(array[i]);
                        break;
                    }
                }
            }
        }
        disableCheckboxes(arrTemp);
    }

    function disableCheckboxes(arrTemp) {
        var cont = 0;
        var checkboxes = getAllCheckboxes();
        for (var i = 0; i < checkboxes.length; i++) {
            for (var j = 0; j < arrTemp.length; j++) {
                if (checkboxes[i].value == arrTemp[j].value) {
                    cont = cont + 1;
                    checkboxes[i].disabled = false;
                    break;
                }
            }
            if (cont === 0) {
                checkboxes[i].disabled = true;
            }
            cont = 0;
        }
    }

}
