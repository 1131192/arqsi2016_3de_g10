namespace ImobiliariaIteracao2.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ImobiliariaIteracao2.DAL.ImobiliariaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ImobiliariaIteracao2.DAL.ImobiliariaContext context)
        {
            using (var context2 = new ApplicationDbContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context2);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                roleManager.Create(new IdentityRole("Admin"));
                roleManager.Create(new IdentityRole("Cliente"));

                var userStore = new UserStore<ApplicationUser>(context2);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var user = new ApplicationUser { UserName = "admin@isep.ipp.pt", Email = "admin@isep.ipp.pt" };

                userManager.Create(user, "Password1!");
                userManager.AddToRole(user.Id, "Admin");

            }
            
        }
    }
}
