namespace ImobiliariaIteracao2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialM : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alerta",
                c => new
                    {
                        AlertaID = c.Int(nullable: false, identity: true),
                        UserID = c.String(),
                        TipoImovelID = c.Int(),
                        TipoAnuncio = c.Int(),
                        Area = c.Single(),
                        Preco = c.Single(),
                        DataCriacao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AlertaID)
                .ForeignKey("dbo.TipoImovel", t => t.TipoImovelID)
                .Index(t => t.TipoImovelID);
            
            CreateTable(
                "dbo.TipoImovel",
                c => new
                    {
                        TipoImovelID = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        SubTipoID = c.Int(),
                    })
                .PrimaryKey(t => t.TipoImovelID)
                .ForeignKey("dbo.TipoImovel", t => t.SubTipoID)
                .Index(t => t.SubTipoID);
            
            CreateTable(
                "dbo.Anuncio",
                c => new
                    {
                        AnuncioID = c.Int(nullable: false, identity: true),
                        UserID = c.String(),
                        ImovelID = c.Int(nullable: false),
                        Preco = c.Single(nullable: false),
                        TipoAnuncio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AnuncioID)
                .ForeignKey("dbo.Imovel", t => t.ImovelID, cascadeDelete: true)
                .Index(t => t.ImovelID);
            
            CreateTable(
                "dbo.Imovel",
                c => new
                    {
                        ImovelID = c.Int(nullable: false, identity: true),
                        UserID = c.String(),
                        TipoImovelID = c.Int(nullable: false),
                        GPSID = c.Int(nullable: false),
                        Area = c.Single(nullable: false),
                        Localizacao = c.String(),
                        emAnuncio = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ImovelID)
                .ForeignKey("dbo.GPS", t => t.GPSID, cascadeDelete: true)
                .ForeignKey("dbo.TipoImovel", t => t.TipoImovelID, cascadeDelete: true)
                .Index(t => t.TipoImovelID)
                .Index(t => t.GPSID);
            
            CreateTable(
                "dbo.Foto",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        url = c.String(),
                        ImovelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Imovel", t => t.ImovelId, cascadeDelete: true)
                .Index(t => t.ImovelId);
            
            CreateTable(
                "dbo.GPS",
                c => new
                    {
                        GPSID = c.Int(nullable: false, identity: true),
                        Latitude = c.Single(nullable: false),
                        Longitude = c.Single(nullable: false),
                        Altitude = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.GPSID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Anuncio", "ImovelID", "dbo.Imovel");
            DropForeignKey("dbo.Imovel", "TipoImovelID", "dbo.TipoImovel");
            DropForeignKey("dbo.Imovel", "GPSID", "dbo.GPS");
            DropForeignKey("dbo.Foto", "ImovelId", "dbo.Imovel");
            DropForeignKey("dbo.Alerta", "TipoImovelID", "dbo.TipoImovel");
            DropForeignKey("dbo.TipoImovel", "SubTipoID", "dbo.TipoImovel");
            DropIndex("dbo.Foto", new[] { "ImovelId" });
            DropIndex("dbo.Imovel", new[] { "GPSID" });
            DropIndex("dbo.Imovel", new[] { "TipoImovelID" });
            DropIndex("dbo.Anuncio", new[] { "ImovelID" });
            DropIndex("dbo.TipoImovel", new[] { "SubTipoID" });
            DropIndex("dbo.Alerta", new[] { "TipoImovelID" });
            DropTable("dbo.GPS");
            DropTable("dbo.Foto");
            DropTable("dbo.Imovel");
            DropTable("dbo.Anuncio");
            DropTable("dbo.TipoImovel");
            DropTable("dbo.Alerta");
        }
    }
}
