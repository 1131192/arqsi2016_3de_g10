﻿using ImobiliariaIteracao2.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ImobiliariaIteracao2.DAL
{
    public class ImobiliariaContext : DbContext
    {
        
        public ImobiliariaContext() : base("ImobiliariaContext")
        {
        }

        public DbSet<Imovel> Imoveis { get; set; }
        public DbSet<TipoImovel> TiposImovel { get; set; }
        public DbSet<Anuncio> Anuncios { get; set; }
        public DbSet<GPS> GPS { get; set; }
        public DbSet<Foto> Fotos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Garante que os nomes das tabelas ficam no singular
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<TipoImovel>().
              HasOptional(t => t.SubTipo).
              WithMany().
              HasForeignKey(st => st.SubTipoID);

        }

        public System.Data.Entity.DbSet<ImobiliariaIteracao2.Models.Alerta> Alertas { get; set; }
    }
}