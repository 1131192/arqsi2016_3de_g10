﻿using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ImobiliariaIteracao2.Controllers
{
    public class HomeController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        public ActionResult Index()
        {/*
            using (var context = new ApplicationDbContext())
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                roleManager.Create(new IdentityRole("Admin"));

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var user = new ApplicationUser { UserName = "admin@isep.ipp.pt" };

                userManager.Create(user, "Password1!");
                userManager.AddToRole(user.Id, "Admin");
            }*/

            if (User.Identity.IsAuthenticated)
            {
                return View();
            }

            return View("../Shared/WidgetHomePageView");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}