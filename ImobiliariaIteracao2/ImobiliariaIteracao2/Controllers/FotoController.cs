﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;

namespace ImobiliariaIteracao2.Controllers
{
    public class FotoController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();

        // GET: Foto
        public ActionResult Index()
        {
            //var fotos = db.Fotos.Include(f => f.Imovel);
            var fotos = getListaFotos();
            return View(fotos.ToList());
        }

        // GET: Foto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Foto foto = db.Fotos.Find(id);
            if (foto == null)
            {
                return HttpNotFound();
            }
            return View(foto);
        }

        // GET: Foto/Create
        public ActionResult Create()
        {
            //ViewBag.ImovelId = new SelectList(db.Imoveis, "ImovelID", "UserID");
            ViewBag.ImovelId = getListaImoveis();
            return View();
        }

        // POST: Foto/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,url,ImovelId")] Foto foto)
        {
            if (ModelState.IsValid)
            {
                db.Fotos.Add(foto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ImovelId = getListaImoveis();
            return View(foto);
        }

        // GET: Foto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Foto foto = db.Fotos.Find(id);
            if (foto == null)
            {
                return HttpNotFound();
            }
            //ViewBag.ImovelId = new SelectList(db.Imoveis, "ImovelID", "UserID", foto.ImovelId);
            ViewBag.ImovelId = getListaImoveis();

            return View(foto);
        }

        // POST: Foto/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,url,ImovelId")] Foto foto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(foto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ImovelId = getListaImoveis();
            return View(foto);
        }

        // GET: Foto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Foto foto = db.Fotos.Find(id);
            if (foto == null)
            {
                return HttpNotFound();
            }
            return View(foto);
        }

        // POST: Foto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Foto foto = db.Fotos.Find(id);
            db.Fotos.Remove(foto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private List<SelectListItem> getListaImoveis()
        {
            // Mostrar apenas os imoveis que o cliente criou
            var temp = db.Imoveis.Include(i => i.GPS).Include(i => i.TipoImovel);
            List<Imovel> imoveis = new List<Imovel>();

            foreach (var imovel in temp)
            {
                if (imovel.UserID == User.Identity.GetUserId())
                {
                    imoveis.Add(imovel);
                }
            }

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var imovel in imoveis)
            {
                string str = imovel.TipoImovel.Nome + ", " +
                                imovel.Area + "m2, " +
                                imovel.Localizacao;

                items.Add(new SelectListItem { Text = str, Value = imovel.ImovelID.ToString() });
            }
            return items;
        }

        private List<Foto> getListaFotos()
        {
            var temp = db.Fotos.Include(f => f.Imovel);
            List<Foto> fotos = new List<Foto>();

            foreach (var item in temp)
            {
                if (item.Imovel.UserID == User.Identity.GetUserId())
                {
                    fotos.Add(item);
                }
            }

            return fotos;
        }
    }
}
