﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Collections;

namespace ImobiliariaIteracao2.Controllers
{
    public class ImovelController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();
        private ImovelRepository repo = new ImovelRepository();
        private GPSRepository repoGPS = new GPSRepository();
        private TipoImovelRepository repoTipoImovel = new TipoImovelRepository();

        // GET: Imovel
        [Authorize]
        public ActionResult Index()
        {
            //var temp = db.Imoveis.Include(i => i.GPS).Include(i => i.TipoImovel);
            var temp = repo.GetData();
            List<Imovel> imoveis = new List<Imovel>();

            foreach (var imovel in temp)
            {
                if (imovel.UserID == User.Identity.GetUserId())
                {
                    imoveis.Add(imovel);
                }
            }
            return View(imoveis.ToList());
        }

        // GET: Imovel/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Imovel imovel = db.Imoveis.Find(id);
            Imovel imovel = repo.GetData(id.Value);

            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // GET: Imovel/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.GPSID = new SelectList(repoGPS.GetData(), "GPSID", "GPSID");
            //ViewBag.TipoImovelID = new SelectList(db.TiposImovel, "TipoImovelID", "Nome");
            ViewBag.TipoImovelID = getTiposImovel();
            //Session["CurrentUrl"] = Request.Url.ToString();
            return View();
        }

        // POST: Imovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "ImovelID,TipoImovelID,GPSID,Area,Localizacao")] Imovel imovel, 
            [Bind(Include = "GPSID,Latitude,Longitude,Altitude")] GPS gps)
        {
            if (ModelState.IsValid)
            {
                // Adicionar o id do cliente ao imóvel
                imovel.UserID = User.Identity.GetUserId();

                // Ao criar o imóvel, ele não pertence a um anúncio
                imovel.emAnuncio = false;

                //db.GPS.Add(gps);
                //db.Imoveis.Add(imovel);
                //db.SaveChanges();
                GPS newGPS = repoGPS.Create(gps);

                imovel.GPSID = newGPS.GPSID;
                repo.Create(imovel);

                return Session["CurrentUrl"] == null ?
                    Redirect("Index") :
                    Redirect(Session["CurrentUrl"].ToString());
            }

            ViewBag.GPSID = new SelectList(repo.GetData(), "GPSID", "GPSID", imovel.GPSID);
            ViewBag.TipoImovelID = getTiposImovel();
            return View(imovel);
        }

        // GET: Imovel/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Imovel imovel = db.Imoveis.Find(id);
            Imovel imovel = repo.GetData(id.Value);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            ViewBag.GPSID = new SelectList(repoGPS.GetData(), "GPSID", "GPSID", imovel.GPSID);
            ViewBag.TipoImovelID = getTiposImovel();
            if (imovel.UserID == User.Identity.GetUserId())
            {
                return View(imovel);
            }
            return View("../Shared/PermissionErrorView");
        }

        // POST: Imovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ImovelID,UserID,TipoImovelID,GPSID,Area,Localizacao,emAnuncio")] Imovel imovel,
            [Bind(Include = "GPSID,Latitude,Longitude,Altitude")] GPS gps)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(gps).State = EntityState.Modified;
                //db.Entry(imovel).State = EntityState.Modified;
                //db.SaveChanges();

                repoGPS.Update(gps.GPSID, gps);
                repo.Update(imovel.ImovelID, imovel);

                return RedirectToAction("Index");
            }
            ViewBag.GPSID = new SelectList(repoGPS.GetData(), "GPSID", "GPSID", imovel.GPSID);
            ViewBag.TipoImovelID = getTiposImovel();
            return View(imovel);
        }

        // GET: Imovel/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Imovel imovel = db.Imoveis.Find(id);
            Imovel imovel = repo.GetData(id.Value);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            if (imovel.UserID == User.Identity.GetUserId())
            {
                return View(imovel);
            }
            return View("../Shared/PermissionErrorView");
        }

        // POST: Imovel/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Imovel imovel = repo.GetData(id);

            //GPS gps = db.GPS.Find(imovel.GPSID);
            //db.Imoveis.Remove(imovel);
            //db.GPS.Remove(gps);
            //db.SaveChanges();

            repoGPS.Delete(imovel.GPSID);
            repo.Delete(id);

            return RedirectToAction("Index");
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private List<SelectListItem> getTiposImovel()
        {
            //var tiposImovel = db.TiposImovel.ToList();
            var tiposImovel = repoTipoImovel.GetData();

            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var tipoImovel in tiposImovel)
            {
                string str;

                if (tipoImovel.SubTipo != null)
                {
                    str = tipoImovel.SubTipo.Nome + ", " +
                                tipoImovel.Nome;
                }
                else
                {
                    str = tipoImovel.Nome;
                }

                items.Add(new SelectListItem { Text = str, Value = tipoImovel.TipoImovelID.ToString() });
            }
            return items;
        }
    }
}
