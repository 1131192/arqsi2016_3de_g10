﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;

namespace ImobiliariaIteracao2.Controllers
{
    public class AlertaController : Controller
    {
        private ImobiliariaContext db = new ImobiliariaContext();
        private AlertaRepository repo = new AlertaRepository();
        private TipoImovelRepository repoTipoImovel = new TipoImovelRepository();

        // GET: Alerta
        public ActionResult Index()
        {
            var temp = repo.GetData();

            List<Alerta> alertas = new List<Alerta>();

            foreach (var alerta in temp)
            {
                if (alerta.UserID == User.Identity.GetUserId())
                {
                    alertas.Add(alerta);
                }
            }

            return View(alertas.ToList());
        }

        // GET: Alerta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Alerta alerta = db.Alertas.Find(id);
            Alerta alerta = repo.GetData(id.Value);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alerta/Create
        public ActionResult Create()
        {
            ViewBag.TipoImovelID = getTiposImovel();
            //ViewBag.TipoImovelID = new SelectList(db.TiposImovel, "TipoImovelID", "Nome");
            return View();
        }

        // POST: Alerta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlertaID,UserID,TipoImovelID,TipoAnuncio,Area,Preco,DataCriacao")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                alerta.DataCriacao = DateTime.Now;
                alerta.UserID = User.Identity.GetUserId();

                //db.Alertas.Add(alerta);
                //db.SaveChanges();

                repo.Create(alerta);
                return RedirectToAction("Index");
            }

            ViewBag.TipoImovelID = getTiposImovel();
            return View(alerta);
        }

        // GET: Alerta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Alerta alerta = db.Alertas.Find(id);
            Alerta alerta = repo.GetData(id.Value);

            if (alerta == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoImovelID = getTiposImovel();
            return View(alerta);
        }

        // POST: Alerta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlertaID,UserID,TipoImovelID,TipoAnuncio,Area,Preco,DataCriacao")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(alerta).State = EntityState.Modified;
                //db.SaveChanges();

                repo.Update(alerta.AlertaID, alerta);
                return RedirectToAction("Index");
            }
            ViewBag.TipoImovelID = getTiposImovel();
            return View(alerta);
        }

        // GET: Alerta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Alerta alerta = db.Alertas.Find(id);
            Alerta alerta = repo.GetData(id.Value);

            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alerta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Alerta alerta = db.Alertas.Find(id);
            //db.Alertas.Remove(alerta);
            //db.SaveChanges();

            repo.Delete(id);
            return RedirectToAction("Index");
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private List<SelectListItem> getTiposImovel()
        {
            var tiposImovel = repoTipoImovel.GetData();
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var tipoImovel in tiposImovel)
            {
                string str;

                if (tipoImovel.SubTipo != null)
                {
                    str = tipoImovel.SubTipo.Nome + ", " +
                                tipoImovel.Nome;
                }
                else
                {
                    str = tipoImovel.Nome;
                }

                items.Add(new SelectListItem { Text = str, Value = tipoImovel.TipoImovelID.ToString() });
            }
            return items;
        }
    }
}
