﻿using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ImobiliariaIteracao2.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private UserRepository repo = new UserRepository();
        private RoleRepository repoRole = new RoleRepository();

        // GET: TipoImovel
        public ActionResult Index()
        {
            var roles = repoRole.GetRoles();
            ViewBag.Roles = roles.ToList();

            return View(repo.GetUsers().ToList());
        }

        // GET: TipoImovel/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var roles = repoRole.GetRoles();
            ViewBag.Roles = roles.ToList();

            ApplicationUser user = repo.GetUser(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: TipoImovel/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AccessFailedCount,Claims,Email,EmailConfirmed,LockoutEnabled,LockoutEndDateUtc,Logins,PasswordHash,PhoneNumber,PhoneNumberConfirmed,Roles,SecurityStamp,TwoFactorEnabled,UserName")] ApplicationUser user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    repo.updateUserRole(user, "Mediador");
                }
                catch (Exception)
                {
                    return HttpNotFound();
                }
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}