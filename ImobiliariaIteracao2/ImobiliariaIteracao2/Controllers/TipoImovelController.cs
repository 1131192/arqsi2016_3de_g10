﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;

namespace ImobiliariaIteracao2.Controllers
{
    public class TipoImovelController : Controller
    {
        private TipoImovelRepository repo = new TipoImovelRepository();

        // GET: TipoImovel
        [Authorize]
        public ActionResult Index()
        {
            return View(repo.GetData().ToList());
        }

        // GET: TipoImovel/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TipoImovel tipoImovel = db.TiposImovel.Find(id);
            TipoImovel tipoImovel = repo.GetData(id.Value);
            if (tipoImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoImovel);
        }

        // GET: TipoImovel/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            //ViewBag.TipoImovelID = new SelectList(db.TiposImovel, "TipoImovelID", "Nome");
            ViewBag.TipoImovelID = new SelectList(repo.GetData(), "TipoImovelID", "Nome");
            return View();
        }

        // POST: TipoImovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoImovelID,Nome,SubTipoID")] TipoImovel tipoImovel)
        {
            if (ModelState.IsValid)
            {
                //db.TiposImovel.Add(tipoImovel);
                //db.SaveChanges();

                repo.Create(tipoImovel);
                return RedirectToAction("Index");
            }

            return View(tipoImovel);
        }

        // GET: TipoImovel/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //TipoImovel tipoImovel = db.TiposImovel.Find(id);
            //ViewBag.TipoImovelID = new SelectList(db.TiposImovel, "TipoImovelID", "Nome");

            TipoImovel tipoImovel = repo.GetData(id.Value);
            //ViewBag.TipoImovelID = new SelectList(repo.GetData(), "TipoImovelID", "Nome");
            ViewBag.TipoImovelID = new SelectList(getListaTipos(id.Value), "TipoImovelID", "Nome");

            if (tipoImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoImovel);
        }

        // POST: TipoImovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoImovelID,Nome,SubTipoID")] TipoImovel tipoImovel)
        {
            if (ModelState.IsValid)
            {
                repo.Update(tipoImovel.TipoImovelID, tipoImovel);

                return RedirectToAction("Index");
            }
            return View(tipoImovel);
        }

        // GET: TipoImovel/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoImovel tipoImovel = repo.GetData(id.Value);

            if (tipoImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoImovel);
        }

        // POST: TipoImovel/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoImovel ti = repo.GetData(id);

            List<TipoImovel> lista = repo.GetData();

            foreach (var tipoImovel in lista)
            {
                if (tipoImovel.SubTipoID == ti.TipoImovelID)
                {
                    return View("../TipoImovel/DeleteErrorView");
                }
            }

            repo.Delete(id);

            return RedirectToAction("Index");
        }

        public List<TipoImovel> getListaTipos(int id)
        {
            List<TipoImovel> lista = repo.GetData();
            List<TipoImovel> listaFinal = new List<TipoImovel>();

            foreach (var ti in lista)
            {
                if (ti.SubTipoID != id && ti.TipoImovelID != id)
                {
                    listaFinal.Add(ti);
                }
            }

            return listaFinal;
        }
    }
}
