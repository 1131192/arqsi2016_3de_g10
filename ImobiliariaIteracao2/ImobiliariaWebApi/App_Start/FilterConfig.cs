﻿using System.Web;
using System.Web.Mvc;

namespace ImobiliariaWebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            //novo
            filters.Add(new RequireHttpsAttribute());

        }
    }
}
