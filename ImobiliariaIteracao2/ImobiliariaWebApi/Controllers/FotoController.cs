﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;

namespace ImobiliariaWebApi.Controllers
{
    public class FotoController : ApiController
    {
        private FotoRepository repo = new FotoRepository();

        // GET: api/Foto
        public IQueryable<Foto> GetFotos()
        {
            return repo.GetData().AsQueryable();
        }

        // GET: api/Foto/5
        [ResponseType(typeof(Foto))]
        public async Task<IHttpActionResult> GetFoto(int id)
        {
            Foto foto = repo.GetData(id);
            if (foto == null)
            {
                return NotFound();
            }

            return Ok(foto);
        }

        // PUT: api/Foto/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFoto(int id, Foto foto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != foto.ID)
            {
                return BadRequest();
            }

            //db.Entry(foto).State = EntityState.Modified;

            try
            {
                repo.Update(id, foto);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FotoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Foto
        [ResponseType(typeof(Foto))]
        public async Task<IHttpActionResult> PostFoto(Foto foto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.Fotos.Add(foto);
            //await db.SaveChangesAsync();
            repo.Create(foto);
            return CreatedAtRoute("DefaultApi", new { id = foto.ID }, foto);
        }

        // DELETE: api/Foto/5
        [ResponseType(typeof(Foto))]
        public async Task<IHttpActionResult> DeleteFoto(int id)
        {
            Foto foto = repo.GetData(id);
            if (foto == null)
            {
                return NotFound();
            }

            //db.Fotos.Remove(foto);
            //await db.SaveChangesAsync();
            repo.Delete(id);
            return Ok(foto);
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private bool FotoExists(int id)
        {
            Foto f = repo.GetData(id);
            if (f == null)
            {
                return false;
            }
            return true;
        }
    }
}