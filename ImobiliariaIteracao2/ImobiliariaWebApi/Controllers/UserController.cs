﻿using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ImobiliariaWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : ApiController
    {
        private UserRepository repo = new UserRepository();

        public IQueryable<ApplicationUser> GetUsers()
        {
            return repo.GetUsers().AsQueryable();
        }

        // GET: api/User/5
        [ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> GetUser(string id)
        {
            ApplicationUser user = repo.GetUser(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(string id, ApplicationUser user)
        {
            try
            {
                repo.updateUserRole(user, "Mediador");
            }
            catch (Exception)
            {
                return StatusCode(HttpStatusCode.NotModified);
            }
            return StatusCode(HttpStatusCode.OK);
        }
    }
}
