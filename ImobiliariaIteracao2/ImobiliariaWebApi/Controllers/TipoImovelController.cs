﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using System.Web;

namespace ImobiliariaWebApi.Controllers
{
    
    public class TipoImovelController : ApiController
    {
        private TipoImovelRepository repo = new TipoImovelRepository();

        // GET: api/TipoImovel
        [Authorize]
        public IQueryable<TipoImovel> GetTiposImovel()
        {
            return repo.GetData().AsQueryable();
        }

        // GET: api/TipoImovel/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(TipoImovel))]
        public async Task<IHttpActionResult> GetTipoImovel(int id)
        {
            TipoImovel tipoImovel = repo.GetData(id);

            if (tipoImovel == null)
            {
                return NotFound();
            }

            //if (HttpContext.Current.User.IsInRole("Admin"))
            //{
                return Ok(tipoImovel);
            //}

            //return Unauthorized();
        }

        // PUT: api/TipoImovel/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoImovel(int id, TipoImovel tipoImovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoImovel.TipoImovelID)
            {
                return BadRequest();
            }

            //db.Entry(tipoImovel).State = EntityState.Modified;

            try
            {
                repo.Update(id, tipoImovel);

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoImovelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoImovel
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(TipoImovel))]
        public async Task<IHttpActionResult> PostTipoImovel(TipoImovel tipoImovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.TiposImovel.Add(tipoImovel);
            //await db.SaveChangesAsync();

            repo.Create(tipoImovel);

            return CreatedAtRoute("DefaultApi", new { id = tipoImovel.TipoImovelID }, tipoImovel);
        }

        // DELETE: api/TipoImovel/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(TipoImovel))]
        public async Task<IHttpActionResult> DeleteTipoImovel(int id)
        {
            TipoImovel tipoImovel = repo.GetData(id);
            if (tipoImovel == null)
            {
                return NotFound();
            }

            //db.TiposImovel.Remove(tipoImovel);
            //await db.SaveChangesAsync();
            if (repo.Delete(id))
            {
                return Ok(tipoImovel);
            }
            return BadRequest();
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private bool TipoImovelExists(int id)
        {
            TipoImovel ti = repo.GetData(id);
            if (ti == null)
            {
                return false;
            }
            return true;
        }
    }
}