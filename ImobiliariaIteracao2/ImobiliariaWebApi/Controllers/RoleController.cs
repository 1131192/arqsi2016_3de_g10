﻿using DataAccessLibrary.DAL;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ImobiliariaWebApi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : ApiController
    {
        private RoleRepository repo = new RoleRepository();

        public IQueryable<IdentityRole> GetRoles()
        {
            return repo.GetRoles().AsQueryable();
        }
    }
}
