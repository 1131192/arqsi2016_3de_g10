﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using System.Web;
using Microsoft.AspNet.Identity;

namespace ImobiliariaWebApi.Controllers
{
    [Authorize]
    public class ImovelController : ApiController
    {
        private ImovelRepository repo = new ImovelRepository();

        // GET: api/Imovel
        public IQueryable<Imovel> GetImoveis()
        {
            // Devolve o id do user que fez o pedido
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            

            return repo.GetData(userId).AsQueryable();
        }

        // GET: api/Imovel/5
        [ResponseType(typeof(Imovel))]
        public async Task<IHttpActionResult> GetImovel(int id)
        {
            Imovel imovel = repo.GetData(id);
            if (imovel == null)
            {
                return NotFound();
            }

            return Ok(imovel);
        }

        // PUT: api/Imovel/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutImovel(int id, Imovel imovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != imovel.ImovelID)
            {
                return BadRequest();
            }

            //db.Entry(imovel).State = EntityState.Modified;

            try
            {
                //await db.SaveChangesAsync();
                repo.Update(id, imovel);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImovelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Imovel
        [ResponseType(typeof(Imovel))]
        public async Task<IHttpActionResult> PostImovel(Imovel imovel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            imovel.UserID = System.Web.HttpContext.Current.User.Identity.GetUserId();
            repo.Create(imovel);

            return CreatedAtRoute("DefaultApi", new { id = imovel.ImovelID }, imovel);
        }

        // DELETE: api/Imovel/5
        [ResponseType(typeof(Imovel))]
        public async Task<IHttpActionResult> DeleteImovel(int id)
        {
            Imovel imovel = repo.GetData(id);
            if (imovel == null)
            {
                return NotFound();
            }

            //db.Imoveis.Remove(imovel);
            //await db.SaveChangesAsync();

            repo.Delete(id);

            return Ok(imovel);
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private bool ImovelExists(int id)
        {
            Imovel i = repo.GetData(id);
            if (i == null)
            {
                return false;
            }
            return true;
        }
    }
}