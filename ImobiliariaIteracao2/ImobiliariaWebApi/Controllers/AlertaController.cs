﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using System.Web;

namespace ImobiliariaWebApi.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class AlertaController : ApiController
    {
        private AlertaRepository repo = new AlertaRepository();

        // GET: api/Alerta
        public IQueryable<Alerta> GetAlertas()
        {
            string userId = HttpContext.Current.User.Identity.GetUserId();
            return repo.GetData(userId).AsQueryable();
        }

        // GET: api/Alerta/5
        [Authorize]
        [ResponseType(typeof(Alerta))]
        public async Task<IHttpActionResult> GetAlerta(int id)
        {
            Alerta alerta = repo.GetData(id);
            if (alerta == null)
            {
                return NotFound();
            }

            return Ok(alerta);
        }

        // PUT: api/Alerta/5
        [Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAlerta(int id, Alerta alerta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != alerta.AlertaID)
            {
                return BadRequest();
            }

            try
            {
                repo.Update(id, alerta);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlertaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Alerta
        [Authorize]
        [ResponseType(typeof(Alerta))]
        public async Task<IHttpActionResult> PostAlerta(Alerta alerta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            alerta.UserID = HttpContext.Current.User.Identity.GetUserId();
            repo.Create(alerta);

            return CreatedAtRoute("DefaultApi", new { id = alerta.AlertaID }, alerta);
        }

        // DELETE: api/Alerta/5
        [Authorize]
        [ResponseType(typeof(Alerta))]
        public async Task<IHttpActionResult> DeleteAlerta(int id)
        {
            Alerta alerta = repo.GetData(id);
            if (alerta == null)
            {
                return NotFound();
            }

            //db.Alertas.Remove(alerta);
            //await db.SaveChangesAsync();

            repo.Delete(id);
            
            return Ok(alerta);
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private bool AlertaExists(int id)
        {
            Alerta a = repo.GetData(id);
            if (a == null)
            {
                return false;
            }
            return true;
        }
    }
}