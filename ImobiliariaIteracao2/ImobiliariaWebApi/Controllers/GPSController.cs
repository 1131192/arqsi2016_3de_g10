﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;

namespace ImobiliariaWebApi.Controllers
{
    public class GPSController : ApiController
    {
        //private ImobiliariaContext db = new ImobiliariaContext();
        private GPSRepository repo = new GPSRepository();

        // GET: api/GPS
        public IQueryable<GPS> GetGPS()
        {
            return repo.GetData().AsQueryable();
        }

        // GET: api/GPS/5
        [ResponseType(typeof(GPS))]
        public async Task<IHttpActionResult> GetGPS(int id)
        {
            GPS gPS = repo.GetData(id);
            if (gPS == null)
            {
                return NotFound();
            }

            return Ok(gPS);
        }

        // PUT: api/GPS/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGPS(int id, GPS gPS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gPS.GPSID)
            {
                return BadRequest();
            }

            //db.Entry(gPS).State = EntityState.Modified;

            try
            {
                repo.Update(id, gPS);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GPSExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GPS
        [ResponseType(typeof(GPS))]
        public async Task<IHttpActionResult> PostGPS(GPS gPS)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.GPS.Add(gPS);
            //await db.SaveChangesAsync();
            repo.Create(gPS);

            return CreatedAtRoute("DefaultApi", new { id = gPS.GPSID }, gPS);
        }

        // DELETE: api/GPS/5
        [ResponseType(typeof(GPS))]
        public async Task<IHttpActionResult> DeleteGPS(int id)
        {
            GPS gPS = repo.GetData(id);
            if (gPS == null)
            {
                return NotFound();
            }

            //db.GPS.Remove(gPS);
            //await db.SaveChangesAsync();
            repo.Delete(id);

            return Ok(gPS);
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/

        private bool GPSExists(int id)
        {
            GPS gps = repo.GetData(id);
            if (gps == null)
            {
                return false;
            }
            return true;
        }
    }
}