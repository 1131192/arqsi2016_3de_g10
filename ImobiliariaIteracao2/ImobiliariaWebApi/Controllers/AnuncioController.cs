﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccessLibrary.DAL;
using DataAccessLibrary.Models;
using System.Web;
using Microsoft.AspNet.Identity;

namespace ImobiliariaWebApi.Controllers
{
    //[Authorize]
    public class AnuncioController : ApiController
    {
        //private ImobiliariaContext db = new ImobiliariaContext();
        private AnuncioRepository repo = new AnuncioRepository();

        // GET: api/Anuncio
        public IQueryable<Anuncio> GetAnuncios()
        {
            return repo.GetData().AsQueryable();
        }

        // GET: api/Anuncio/5
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> GetAnuncio(int id)
        {
            Anuncio anuncio = repo.GetData(id);
            if (anuncio == null)
            {
                return NotFound();
            }

            //if (anuncio.UserID == HttpContext.Current.User.Identity.GetUserId())
            //{
                return Ok(anuncio);
            //}
            //return Unauthorized();
        }

        // PUT: api/Anuncio/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAnuncio(int id, Anuncio anuncio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != anuncio.AnuncioID)
            {
                return BadRequest();
            }

            try
            {
                repo.Update(id, anuncio);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnuncioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Anuncio
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> PostAnuncio(Anuncio anuncio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            anuncio.UserID = HttpContext.Current.User.Identity.GetUserId();
            repo.Create(anuncio);

            return CreatedAtRoute("DefaultApi", new { id = anuncio.AnuncioID }, anuncio);
        }

        // DELETE: api/Anuncio/5
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> DeleteAnuncio(int id)
        {
           /*Anuncio anuncio = await db.Anuncios.FindAsync(id);
            if (anuncio == null)
            {
                return NotFound();
            }*/

            //db.Anuncios.Remove(anuncio);
            repo.Delete(id);
            //await db.SaveChangesAsync();

            //return Ok(anuncio);
            return Ok();
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/
        
        private bool AnuncioExists(int id)
        {
            //return db.Anuncios.Count(e => e.AnuncioID == id) > 0;
            Anuncio a = repo.GetData(id);
            if (a == null)
            {
                return false;
            }
            return true;
        }
    }
}