﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    interface IAnuncioRepository
    {
        List<Anuncio> GetData();
        List<Anuncio> GetData(string userId);
        Anuncio GetData(int id);
        Anuncio Create(Anuncio a);
        bool Update(int id, Anuncio a);
        bool Delete(int id);
    }
}
