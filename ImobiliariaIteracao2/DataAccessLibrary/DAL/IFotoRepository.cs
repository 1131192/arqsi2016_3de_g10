﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    interface IFotoRepository
    {
        List<Foto> GetData();
        Foto GetData(int id);
        Foto Create(Foto foto);
        bool Update(int id, Foto foto);
        bool Delete(int id);
    }
}
