﻿using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    public class UserRepository
    {
        private ApplicationDbContext context;
        private RoleRepository repoRole = new RoleRepository();

        public UserRepository()
        {
            context = new ApplicationDbContext();
        }

        public List<ApplicationUser> GetUsers()
        {
            List<ApplicationUser> usersList = new List<ApplicationUser>();

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var users = (from u in userManager.Users select u).ToList();

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var role = roleManager.FindByName("Admin");

            var count = 0;

            foreach (var u in users)
            {
                foreach (var r in u.Roles)
                {
                    if (r.RoleId == role.Id)
                    {
                        count++;
                    }
                }
                if (count == 0)
                {
                    usersList.Add(u);
                }
                count = 0;
            }

            return usersList;
        }

        public ApplicationUser GetUser(string Id)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            return userManager.FindById(Id);
        }

        public void updateUserRole(ApplicationUser user, string roleName)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var u = userManager.FindById(user.Id);

            if (u == null)
                throw new Exception("User not found!");

            var role = roleManager.FindByName(roleName);
            if (role == null)
                throw new Exception("Role not found!");

            if (userManager.IsInRole(user.Id, role.Name))
            {
                this.DeleteRoleForUser(user.Id, role.Name);
            }
            else
            {
                userManager.AddToRole(user.Id, role.Name);
                context.SaveChanges();

            }
        }

        private void DeleteRoleForUser(string userId, string roleName)
        {
            List<string> roles;
            List<string> users;

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            roles = (from r in roleManager.Roles select r.Name).ToList();

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            users = (from u in userManager.Users select u.Id).ToList();

            var user = userManager.FindById(userId);
            if (user == null)
                throw new Exception("User not found!");

            if (userManager.IsInRole(user.Id, roleName))
            {
                userManager.RemoveFromRole(user.Id, roleName);
                context.SaveChanges();
            }
            else
            {
                throw new Exception("User not in role!");
            }
        }

        public void updateUser(ApplicationUser user)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var u = userManager.FindById(user.Id);
            if (u != null)
            {
                u = user;
            }
        }
    }
}
