﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.Models;

namespace DataAccessLibrary.DAL
{
    public class AlertaRepository : IAlertaRepository
    {
        public ImobiliariaContext context;

        public AlertaRepository()
        {
            context = new ImobiliariaContext();
        }

        public Alerta Create(Alerta a)
        {
            context.Alertas.Add(a);
            context.SaveChanges();
            return a;
        }

        public bool Delete(int id)
        {
            var alerta = context.Alertas.Find(id);
            if (alerta != null)
            {
                context.Alertas.Remove(alerta);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<Alerta> GetData()
        {
            var alertas = context.Alertas.ToList();
            return alertas;
        }

        public List<Alerta> GetData(string userId)
        {
            var alertas = context.Alertas.ToList();
            if (userId != null)
            {
                List<Alerta> lista = new List<Alerta>();

                foreach (var a in alertas)
                {
                    if (a.UserID == userId)
                    {
                        lista.Add(a);
                    }
                }
                return lista;
            }
            return alertas;
        }

        public Alerta GetData(int id)
        {
            return context.Alertas.Find(id);
        }

        public bool Update(int id, Alerta a)
        {
            var alerta = context.Alertas.Find(id);
            if (alerta != null)
            {
                alerta.TipoImovelID = a.TipoImovelID;
                alerta.TipoAnuncio = a.TipoAnuncio;
                alerta.Preco = a.Preco;
                alerta.Area = a.Area;

                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
