﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.Models;

namespace DataAccessLibrary.DAL
{
    public class TipoImovelRepository : ITipoImovelRepository
    {
        public ImobiliariaContext context;

        public TipoImovelRepository()
        {
            context = new ImobiliariaContext();
        }

        public TipoImovel Create(TipoImovel ti)
        {
            context.TiposImovel.Add(ti);
            context.SaveChanges();
            return ti;
        }

        public bool Delete(int id)
        {
            var tipoImovel = context.TiposImovel.Find(id);
            if (tipoImovel != null)
            {
                List<TipoImovel> lista = this.GetData();
                foreach (var t in lista)
                {
                    if (t.SubTipoID == tipoImovel.TipoImovelID)
                    {
                        return false;
                    }
                }
                context.TiposImovel.Remove(tipoImovel);
                context.SaveChanges();
                return true;
            }

            return false;
        }

        public List<TipoImovel> GetData()
        {
            var tiposImovel = context.TiposImovel.ToList();
            return tiposImovel;
        }

        public TipoImovel GetData(int id)
        {
            return context.TiposImovel.Find(id);
        }

        public bool Update(int id, TipoImovel ti)
        {
            var tipoImovel = context.TiposImovel.Find(id);
            if (tipoImovel != null)
            {
                tipoImovel.Nome = ti.Nome;
                tipoImovel.SubTipoID = ti.SubTipoID;

                context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
