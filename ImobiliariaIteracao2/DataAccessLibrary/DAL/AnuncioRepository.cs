﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.Models;

namespace DataAccessLibrary.DAL
{
    public class AnuncioRepository : IAnuncioRepository
    {
        public ImobiliariaContext context;

        public AnuncioRepository()
        {
            context = new ImobiliariaContext();
        }

        public Anuncio Create(Anuncio a)
        {
            context.Anuncios.Add(a);
            context.SaveChanges();
            return a;
        }

        public bool Delete(int id)
        {
            var anuncio = context.Anuncios.Find(id);
            if (anuncio != null)
            {
                context.Anuncios.Remove(anuncio);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<Anuncio> GetData()
        {
            var anuncios = context.Anuncios.ToList();
            return anuncios;
        }

        public List<Anuncio> GetData(string userId)
        {
            var anuncios = context.Anuncios.ToList();
            if (userId != null)
            {
                List<Anuncio> lista = new List<Anuncio>();

                foreach (var a in anuncios)
                {
                    if (a.UserID == userId)
                    {
                        lista.Add(a);
                    }
                }
                return lista;
            }
            return anuncios;
        }

        public Anuncio GetData(int id)
        {
            return context.Anuncios.Find(id);
        }

        public bool Update(int id, Anuncio a)
        {
            var anuncio = context.Anuncios.Find(id);
            if (anuncio != null)
            {
                anuncio.ImovelID = a.ImovelID;
                anuncio.Preco = a.Preco;
                anuncio.TipoAnuncio = a.TipoAnuncio;
                anuncio.AnuncioCorrID = a.AnuncioCorrID;
                anuncio.Estado = a.Estado;

                context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
