﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    interface IImovelRepository
    {
        List<Imovel> GetData();
        List<Imovel> GetData(string userId);
        Imovel GetData(int id);
        Imovel Create(Imovel i);
        bool Update(int id, Imovel i);
        bool Delete(int id);
    }
}
