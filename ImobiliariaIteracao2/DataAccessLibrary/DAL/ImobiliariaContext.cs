﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    public class ImobiliariaContext : DbContext
    {

        public ImobiliariaContext() : base("ImobiliariaContext")
        {
        }

        public DbSet<Imovel> Imoveis { get; set; }
        public DbSet<TipoImovel> TiposImovel { get; set; }
        public DbSet<Anuncio> Anuncios { get; set; }
        public DbSet<GPS> GPS { get; set; }
        public DbSet<Foto> Fotos { get; set; }
        public DbSet<Alerta> Alertas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Garante que os nomes das tabelas ficam no singular
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<TipoImovel>().
                HasOptional(t => t.SubTipo).
                WithMany().
                HasForeignKey(st => st.SubTipoID);
            /*
            modelBuilder.Entity<Anuncio>().
                HasOptional(a => a.AnuncioCorr);*/
        }
    }
}
