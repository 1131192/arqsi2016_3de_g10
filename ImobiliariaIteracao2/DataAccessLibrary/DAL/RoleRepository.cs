﻿using DataAccessLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    public class RoleRepository
    {
        public ApplicationDbContext context;

        public RoleRepository()
        {
            context = new ApplicationDbContext();
        }

        public void start()
        {
            this.CreateAdmin();
            this.CreateRole("Mediador");
            this.CreateRole("Cliente");
        }

        public IdentityRole CreateRole(IdentityRole role)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            if (!roleManager.RoleExists(role.Name))
            {
                roleManager.Create(role);
                context.SaveChanges();
            }

            return role;
        }

        public IdentityRole CreateRole(string roleName)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            IdentityRole role = new IdentityRole();

            if (roleManager.FindByName(roleName) == null)
            {
                role = new IdentityRole(roleName);
                roleManager.Create(role);
                context.SaveChanges();
            }

            return role;
        }

        public bool DeleteRole(string roleName)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var role = roleManager.FindByName(roleName);

            if (role != null)
            {
                roleManager.Delete(role);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public void AddUserToRole(string userName, string roleName)
        {
            List<string> users;

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            users = (from u in userManager.Users select u.UserName).ToList();

            var user = userManager.FindByName(userName);
            if (user == null)
                throw new Exception("User not found!");

            var role = roleManager.FindByName(roleName);
            if (role == null)
                throw new Exception("Role not found!");

            if (userManager.IsInRole(user.Id, role.Name))
            {
                throw new Exception("Already in role!");
            }
            else
            {
                userManager.AddToRole(user.Id, role.Name);
                context.SaveChanges();
            }
        }

        public List<IdentityRole> GetRoles()
        {
            List<IdentityRole> roles;
            
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            roles = (from r in roleManager.Roles select r).ToList();

            return roles;
        }

        public List<IdentityRole> GetRoles(string userName)
        {
            List<IdentityRole> userRoles = new List<IdentityRole>();
            List<IdentityRole> roles;
            List<string> users;

            if (!string.IsNullOrWhiteSpace(userName))
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                roles = (from r in roleManager.Roles select r).ToList();

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                users = (from u in userManager.Users select u.UserName).ToList();

                var user = userManager.FindByName(userName);
                if (user == null)
                    throw new Exception("User not found!");

                var userRoleIds = (from r in user.Roles select r.RoleId);
                userRoles = (from id in userRoleIds
                                let r = roleManager.FindById(id)
                                select r).ToList();

            }

            return userRoles;
        }

        public void DeleteRoleForUser(string userName, string roleName)
        {
            List<string> roles;
            List<string> users;
            
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            roles = (from r in roleManager.Roles select r.Name).ToList();

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            users = (from u in userManager.Users select u.UserName).ToList();

            var user = userManager.FindByName(userName);
            if (user == null)
                throw new Exception("User not found!");

            if (userManager.IsInRole(user.Id, roleName))
            {
                userManager.RemoveFromRole(user.Id, roleName);
                context.SaveChanges();
            }
            else
            {
                throw new Exception("User not in role!");
            }
        }

        public void CreateAdmin()
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var admin = userManager.FindByEmail("admin@isep.ipp.pt");
            if (admin == null)
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                roleManager.Create(new IdentityRole("Admin"));

                var user = new ApplicationUser { UserName = "admin@isep.ipp.pt", Email = "admin@isep.ipp.pt" };

                userManager.Create(user, "Password1!");
                userManager.AddToRole(user.Id, "Admin");

                context.SaveChanges();
            }
        }

    }
}
