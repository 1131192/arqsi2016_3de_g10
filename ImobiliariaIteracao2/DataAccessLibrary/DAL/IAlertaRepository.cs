﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    interface IAlertaRepository
    {
        List<Alerta> GetData();
        List<Alerta> GetData(string userId);
        Alerta GetData(int id);
        Alerta Create(Alerta a);
        bool Update(int id, Alerta a);
        bool Delete(int id);
    }
}
