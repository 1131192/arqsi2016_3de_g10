﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.Models;

namespace DataAccessLibrary.DAL
{
    public class FotoRepository : IFotoRepository
    {
        private ImobiliariaContext context;

        public FotoRepository()
        {
            context = new ImobiliariaContext();
        }

        public Foto Create(Foto foto)
        {
            context.Fotos.Add(foto);
            context.SaveChanges();
            return foto;
        }

        public bool Delete(int id)
        {
            var foto = context.Fotos.Find(id);
            if (foto != null)
            {
                context.Fotos.Remove(foto);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<Foto> GetData()
        {
            var fotos = context.Fotos.ToList();
            return fotos;
        }

        public Foto GetData(int id)
        {
            return context.Fotos.Find(id);
        }

        public bool Update(int id, Foto foto)
        {
            var f = context.Fotos.Find(id);
            if (f != null)
            {
                f.ImovelId = foto.ImovelId;
                f.url = foto.url;

                context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
