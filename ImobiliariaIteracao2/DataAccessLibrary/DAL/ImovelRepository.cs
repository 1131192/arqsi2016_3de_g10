﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.Models;

namespace DataAccessLibrary.DAL
{
    public class ImovelRepository : IImovelRepository
    {
        public ImobiliariaContext context;

        public ImovelRepository()
        {
            context = new ImobiliariaContext();
        }

        public Imovel Create(Imovel i)
        {
            context.Imoveis.Add(i);
            context.SaveChanges();
            return i;
        }

        public bool Delete(int id)
        {
            var imovel = context.Imoveis.Find(id);
            if (imovel != null)
            {
                context.Imoveis.Remove(imovel);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<Imovel> GetData()
        {
            var imoveis = context.Imoveis.ToList();
            return imoveis;
        }

        public List<Imovel> GetData(string userId)
        {
            var imoveis = context.Imoveis.ToList();
            
            if (userId != null)
            {
                List<Imovel> lista = new List<Imovel>();
                foreach (var i in imoveis)
                {
                    if (i.UserID == userId)
                    {
                        lista.Add(i);
                    }
                }
                return lista;
            }

            return imoveis;
        }

        public Imovel GetData(int id)
        {
            return context.Imoveis.Find(id);
        }

        public bool Update(int id, Imovel i)
        {
            var imovel = context.Imoveis.Find(id);
            if (imovel != null)
            {
                imovel.Area = i.Area;
                imovel.emAnuncio = i.emAnuncio;
                imovel.Fotos = i.Fotos;
                imovel.GPSID = i.GPSID;
                imovel.Localizacao = i.Localizacao;
                imovel.TipoImovelID = i.TipoImovelID;

                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
