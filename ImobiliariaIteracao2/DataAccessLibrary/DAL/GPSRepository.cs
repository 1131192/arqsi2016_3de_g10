﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLibrary.Models;

namespace DataAccessLibrary.DAL
{
    public class GPSRepository : IGPSRepository
    {
        public ImobiliariaContext context;

        public GPSRepository()
        {
            context = new ImobiliariaContext();
        }

        public GPS Create(GPS gps)
        {
            context.GPS.Add(gps);
            context.SaveChanges();
            return gps;
        }

        public bool Delete(int id)
        {
            var gps = context.GPS.Find(id);
            if (gps != null)
            {
                context.GPS.Remove(gps);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<GPS> GetData()
        {
            var gps = context.GPS.ToList();
            return gps;
        }

        public GPS GetData(int id)
        {
            return context.GPS.Find(id);
        }

        public bool Update(int id, GPS gps)
        {
            var gps2 = context.GPS.Find(id);
            if (gps2 != null)
            {
                gps2.Latitude = gps.Latitude;
                gps2.Longitude = gps.Longitude;
                gps2.Altitude = gps.Altitude;

                context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
