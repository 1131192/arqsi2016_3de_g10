﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    interface ITipoImovelRepository
    {
        List<TipoImovel> GetData();
        TipoImovel GetData(int id);
        TipoImovel Create(TipoImovel ti);
        bool Update(int id, TipoImovel ti);
        bool Delete(int id);
    }
}
