﻿using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary.DAL
{
    interface IGPSRepository
    {
        List<GPS> GetData();
        GPS GetData(int id);
        GPS Create(GPS gps);
        bool Update(int id, GPS gps);
        bool Delete(int id);
    }
}
