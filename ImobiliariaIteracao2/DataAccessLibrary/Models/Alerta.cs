﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DataAccessLibrary.Models
{
    public class Alerta
    {
        public int AlertaID { get; set; }

        public string UserID { get; set; }

        [Display(Name = "Tipo Imóvel")]
        public int? TipoImovelID { get; set; }

        [Display(Name = "Tipo de Anúncio")]
        public TipoAnuncio? TipoAnuncio { get; set; }

        [Display(Name = "Área")]
        public float? Area { get; set; }

        [Display(Name = "Preço")]
        public float? Preco { get; set; }

        [Display(Name = "Data de Criação")]
        public DateTime DataCriacao { get; set; }

        public virtual TipoImovel TipoImovel { get; set; }
    }
}