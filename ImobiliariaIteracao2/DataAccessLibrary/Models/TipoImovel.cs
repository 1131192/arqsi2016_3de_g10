﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLibrary.Models
{
    public class TipoImovel
    {
        public int TipoImovelID { get; set; }

        [Display(Name="Tipo Imóvel")]
        public string Nome { get; set; }
        
        public int? SubTipoID { get; set; }

        public virtual TipoImovel SubTipo { get; set; }
    }
}